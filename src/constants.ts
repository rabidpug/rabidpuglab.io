export const {
  REACT_APP_GAPI_KEY: apiKey,
  REACT_APP_GAPI_CLIENT_ID: clientId,
  REACT_APP_SHEET_ID: spreadsheetId,
} = process.env as { [key: string]: string };
export const range = 'Settings!A:B';
export const scope = 'https://www.googleapis.com/auth/spreadsheets';
export const discoveryDocs = [
  'https://sheets.googleapis.com/$discovery/rest?version=v4',
];
