import { Divider, Paper, Typography } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';

import CreateForm from './CreateForm';
import CreateInitial from './CreateInitial';
import Existing from './Existing';
import Loading from './Loading';
import useDO from './useDO';
import useSettings from './useSettings';

const StyledPaper = styled(Paper)`
  margin-top: ${({ theme }) => theme.spacing(3)}px;
  margin-bottom: ${({ theme }) => theme.spacing(3)}px;
  padding: ${({ theme }) => theme.spacing(2)}px;
  ${({ theme }) => theme.breakpoints.up(600 + theme.spacing(3) * 2)} {
    margin-top: ${({ theme }) => theme.spacing(6)}px;
    margin-bottom: ${({ theme }) => theme.spacing(6)}px;
    padding: ${({ theme }) => theme.spacing(3)}px;
  }
`;
const Main = styled.main`
  width: auto;
  margin-left: ${({ theme }) => theme.spacing(2)}px;
  margin-right: ${({ theme }) => theme.spacing(2)}px;
  ${({ theme }) => theme.breakpoints.up(600 + theme.spacing(2) * 2)} {
    width: 600px;
    margin-left: auto;
    margin-right: auto;
  }
`;
const StyledDivider = styled(Divider)`
  margin-bottom: ${({ theme }) => theme.spacing(2)}px;
`;
export default function Create() {
  const { settings, setSettings, loading } = useSettings();
  const {
    existing,
    loading: doLoading,
    options,
    createDroplet,
    destroyExisting,
  } = useDO(settings?.doAuthToken);

  if (loading || doLoading) return <Loading />;

  return (
    <Main>
      <StyledPaper>
        <Typography component="h1" variant="h4" align="center">
          {existing
            ? 'Existing Droplet'
            : settings && options
            ? 'Create Droplet'
            : 'Enter Token'}
        </Typography>
        <StyledDivider />
        {existing.length > 0 ? (
          <>
            {existing.map(droplet => (
              <Existing
                key={droplet.id}
                droplet={droplet}
                destroyExisting={destroyExisting}
              />
            ))}
          </>
        ) : settings && options ? (
          <CreateForm
            initialValues={settings}
            setSettings={setSettings}
            options={options}
            createDroplet={createDroplet}
          />
        ) : (
          <CreateInitial setSettings={setSettings} />
        )}
      </StyledPaper>
    </Main>
  );
}
