import {
  FormControl,
  FormGroup,
  FormLabel,
  Grid,
  IconButton,
  InputLabel,
  LinearProgress,
  MenuItem,
} from '@material-ui/core';
import { Add, Delete } from '@material-ui/icons';
import { ICreateDropletApiRequest } from 'dots-wrapper/dist/modules/droplet';
import { Field, FieldArray, Form, Formik } from 'formik';
import { CheckboxWithLabel, Select, TextField } from 'formik-material-ui';
import React, { FC, Fragment } from 'react';
import styled from 'styled-components';

import { ButtonWrapper, StyledButton } from './Button';
import { Options } from './useDO';
import { CompleteSettings, InitialSettings, SetSettings } from './useSettings';

interface CreateFormProps {
  initialValues: InitialSettings | CompleteSettings;
  setSettings: SetSettings;
  options: Options;
  createDroplet: (options: ICreateDropletApiRequest) => any;
}

const defaultSettings: Omit<CompleteSettings, 'doAuthToken'> = {
  name: '',
  region: '',
  size: '',
  image: 0,
  ssh_keys: [],
  backups: false,
  ipv6: false,
  private_networking: false,
  monitoring: false,
  volumes: [],
  tag: '',
  variables: [],
};
function checkChanged<T>(obj1: T, obj2: T) {
  return Object.entries(obj1).some(([key, value]) => {
    const initialValue =
      obj2[key as keyof T] ||
      defaultSettings[key as keyof typeof defaultSettings];
    if (Array.isArray(value) && Array.isArray(initialValue)) {
      return !(
        value.length === initialValue.length &&
        initialValue.every((val: any) => value.includes(val))
      );
    }
    return initialValue !== value;
  });
}
const addQuote = (value: string) =>
  value.includes('\n') || value.includes(' ') ? '"' : '';
const StyledFormGroup = styled(FormGroup)`
  justify-content: center;
`;
const CreateForm: FC<CreateFormProps> = ({
  initialValues,
  setSettings,
  options,
  createDroplet,
}) => {
  return (
    <Formik
      initialValues={{ ...defaultSettings, ...initialValues }}
      onSubmit={(values, { setSubmitting }) => {
        if (
          checkChanged<InitialSettings | CompleteSettings>(
            values,
            initialValues,
          )
        )
          setSettings(values);
        else {
          const selectedRegion = options.regions.find(
            ({ slug }) => slug === values.region,
          );
          const isDisabled = (key: string) =>
            !selectedRegion ? false : !selectedRegion.features.includes(key);
          createDroplet({
            name: values.name,
            image: values.image,
            region: values.region,
            size: values.size,
            backups: isDisabled('backups') ? false : values.backups,
            ipv6: isDisabled('ipv6') ? false : values.ipv6,
            private_networking: isDisabled('private_networking')
              ? false
              : values.private_networking,
            monitoring: isDisabled('install_agent') ? false : values.monitoring,
            ssh_keys: values.ssh_keys,
            tags: values.tag ? [values.tag] : [],
            user_data: `#!/bin/bash
set -eou pipefail

BS_PATH=/usr/local/bootstrap

# Clone bootstrap repo to /usr/local/bootstrap and set up
mkdir -p "$BS_PATH"
git clone -q https://gitlab.com/rabidpug/bootstrap.git "$BS_PATH"
touch "$BS_PATH/.env"

# Define required variables for scripts
cat <<EOT >>$BS_PATH/.env
${values.variables.map(({ name, value }) =>
  value.includes(',')
    ? `${name}=(
      ${value.replace(
        /,/g,
        `
  `,
      )}
      )`
    : `${name}=${addQuote(value)}${value.replace(
        /\n/g,
        `
    `,
      )}${addQuote(value)}`,
).join(`
`)}
EOT

touch /var/log/bs.log
bash "$BS_PATH/bs" &>>/var/log/bs.log

            `,
          });
        }
      }}
    >
      {({ submitForm, isSubmitting, values }) => {
        const selectedRegion = options.regions.find(
          ({ slug }) => slug === values.region,
        );
        const isDisabled = (key: string) =>
          !selectedRegion ? false : !selectedRegion.features.includes(key);
        return (
          <Grid container spacing={3} component={Form}>
            <Grid item xs={12}>
              <Field
                name="doAuthToken"
                label="DO Auth Token"
                component={TextField}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Field name="name" label="Name" component={TextField} fullWidth />
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl fullWidth>
                <InputLabel htmlFor="region" shrink>
                  Region
                </InputLabel>
                <Field
                  name="region"
                  component={Select}
                  inputProps={{ id: 'region' }}
                  fullWidth
                >
                  {options.regions.map(region => (
                    <MenuItem value={region.slug} key={region.slug}>
                      {region.name}
                    </MenuItem>
                  ))}
                </Field>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <InputLabel htmlFor="size" shrink>
                  Size
                </InputLabel>
                <Field
                  name="size"
                  component={Select}
                  inputProps={{ id: 'size' }}
                  fullWidth
                >
                  {options.sizes
                    .filter(({ slug, available }) => {
                      if (!available) return false;
                      if (!selectedRegion) return true;
                      return !!selectedRegion.sizes.find(
                        sizeSlug => sizeSlug === slug,
                      );
                    })
                    .map(size => (
                      <MenuItem
                        value={size.slug}
                        key={size.slug}
                      >{`${size.memory / 1024}GB Memory - ${
                        size.vcpus
                      } vcpus - ${size.disk}GB Disk - $${
                        size.price_monthly
                      }/m`}</MenuItem>
                    ))}
                </Field>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl fullWidth>
                <InputLabel htmlFor="image" shrink>
                  Image
                </InputLabel>
                <Field
                  name="image"
                  component={Select}
                  inputProps={{ id: 'image' }}
                  fullWidth
                >
                  {options.images
                    .filter(({ status, regions }) => {
                      if (status !== 'available') return false;
                      return (
                        !selectedRegion || regions.includes(selectedRegion.slug)
                      );
                    })
                    .map(image => (
                      <MenuItem value={image.id} key={image.id}>
                        {`${image.distribution} ${image.name}`}
                      </MenuItem>
                    ))}
                </Field>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormControl fullWidth>
                <InputLabel htmlFor="ssh_keys" shrink>
                  SSH Keys
                </InputLabel>
                <Field
                  name="ssh_keys"
                  component={Select}
                  inputProps={{ id: 'ssh_keys', multiple: true }}
                  fullWidth
                >
                  {options.ssh_keys.map(key => (
                    <MenuItem value={key.id} key={key.id}>
                      {key.name}
                    </MenuItem>
                  ))}
                </Field>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <Field name="tag" label="Tag" component={TextField} fullWidth />
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <FormLabel component="legend">Options</FormLabel>
                <StyledFormGroup row>
                  <Field
                    name="backups"
                    Label={{ label: 'Backups' }}
                    component={CheckboxWithLabel}
                    type="checkbox"
                    disabled={isDisabled('backups')}
                  />
                  <Field
                    name="ipv6"
                    Label={{ label: 'IPV6' }}
                    component={CheckboxWithLabel}
                    type="checkbox"
                    disabled={isDisabled('ipv6')}
                  />
                  <Field
                    name="private_networking"
                    Label={{ label: 'Private Networking' }}
                    component={CheckboxWithLabel}
                    type="checkbox"
                    disabled={isDisabled('private_networking')}
                  />
                  <Field
                    name="monitoring"
                    Label={{ label: 'Monitoring' }}
                    component={CheckboxWithLabel}
                    type="checkbox"
                    disabled={isDisabled('install_agent')}
                  />
                </StyledFormGroup>
              </FormControl>
            </Grid>
            <FieldArray name="variables">
              {({ remove, push }) => (
                <>
                  <Grid item xs={12}>
                    <FormLabel component="legend">Variables</FormLabel>
                  </Grid>
                  {values.variables.map((_, i) => (
                    <Fragment key={i}>
                      <Grid item xs={12} sm={4}>
                        <Field
                          name={`variables[${i}].name`}
                          label="Name"
                          component={TextField}
                          fullWidth
                        />
                      </Grid>
                      <Grid item xs={11} sm={7}>
                        <Field
                          name={`variables[${i}].value`}
                          label="Value"
                          component={TextField}
                          fullWidth
                        />
                      </Grid>
                      <Grid item xs={1}>
                        <IconButton onClick={() => remove(i)} size="small">
                          <Delete />
                        </IconButton>
                      </Grid>
                    </Fragment>
                  ))}
                  <ButtonWrapper>
                    <IconButton
                      onClick={() => push({ name: '', value: '' })}
                      size="small"
                    >
                      <Add />
                    </IconButton>
                  </ButtonWrapper>
                </>
              )}
            </FieldArray>
            {isSubmitting && <LinearProgress />}
            <ButtonWrapper>
              <StyledButton
                variant="contained"
                color="primary"
                disabled={isSubmitting}
                onClick={submitForm}
              >
                {checkChanged<InitialSettings | CompleteSettings>(
                  values,
                  initialValues,
                )
                  ? 'Save'
                  : 'Create'}
              </StyledButton>
            </ButtonWrapper>
          </Grid>
        );
      }}
    </Formik>
  );
};
export default CreateForm;
