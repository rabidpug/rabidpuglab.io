import { CssBaseline, useMediaQuery } from '@material-ui/core';
import {
  createMuiTheme,
  StylesProvider,
  ThemeProvider,
} from '@material-ui/core/styles';
import React, { FC, useMemo } from 'react';
import { ThemeProvider as SCThemeProvider } from 'styled-components';

const Theme: FC = ({ children }) => {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const theme = useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: prefersDarkMode ? 'dark' : 'light',
        },
      }),
    [prefersDarkMode],
  );
  return (
    <ThemeProvider theme={theme}>
      <StylesProvider injectFirst>
        <CssBaseline />
        <SCThemeProvider theme={theme}>{children}</SCThemeProvider>
      </StylesProvider>
    </ThemeProvider>
  );
};
export default Theme;
