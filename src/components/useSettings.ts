import { useEffect, useState } from 'react';

import { range, spreadsheetId } from '../constants';

export interface InitialSettings {
  doAuthToken: string;
}
export interface Variable {
  name: string;
  value: string;
}
export type CompleteSettings = {
  name: string;
  region: string;
  size: string;
  image: number;
  ssh_keys: number[];
  backups: boolean;
  ipv6: boolean;
  private_networking: boolean;
  monitoring: boolean;
  volumes: string[];
  tag: string;
  variables: Variable[];
} & InitialSettings;
type Settings = undefined | InitialSettings | CompleteSettings;
export type SetSettings = (
  newSettings: InitialSettings | CompleteSettings,
) => PromiseLike<void>;
interface UserSettingsType {
  settings: Settings;
  setSettings: SetSettings;
  loading: boolean;
}
export default function useSettings(): UserSettingsType {
  const [settings, saveSettings] = useState<Settings>();
  const [loading, setLoading] = useState(false);
  const getSettings = async () => {
    setLoading(true);
    const { result } = await gapi.client.sheets.spreadsheets.values.get({
      spreadsheetId,
      range,
    });
    if (!result?.values) saveSettings(undefined);
    const newSettings = result?.values?.reduce(
      (obj: InitialSettings | CompleteSettings, [key, value]) => ({
        ...obj,
        [key]: JSON.parse(value),
      }),
      {} as InitialSettings | CompleteSettings,
    );
    saveSettings(newSettings);
    setLoading(false);
  };
  useEffect(() => {
    getSettings();
    return () => saveSettings(undefined);
  }, []);
  const setSettings = async (
    newSettings: InitialSettings | CompleteSettings,
  ) => {
    setLoading(true);
    await gapi.client.sheets.spreadsheets.values.update({
      spreadsheetId,
      range,
      valueInputOption: 'RAW',
      resource: {
        values: Object.entries(newSettings).map(([key, value]) => [
          key,
          JSON.stringify(value),
        ]),
      },
    });
    saveSettings(newSettings);
    setLoading(false);
  };
  return { settings, setSettings, loading };
}
