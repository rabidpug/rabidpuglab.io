import { Button, LinearProgress } from '@material-ui/core';
import { Field, Form, Formik } from 'formik';
import { TextField } from 'formik-material-ui';
import React, { FC } from 'react';

import { InitialSettings, SetSettings } from './useSettings';

interface CreateFormProps {
  setSettings: SetSettings;
}

const initialValues: InitialSettings = { doAuthToken: '' };
const CreateInitial: FC<CreateFormProps> = ({ setSettings }) => {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={(values, { setSubmitting }) => {
        setSettings(values);
      }}
    >
      {({ submitForm, isSubmitting, values }) => (
        <Form style={{ width: '100%' }}>
          <Field
            name="doAuthToken"
            label="DO Auth Token"
            component={TextField}
            fullWidth
          />
          {isSubmitting && <LinearProgress />}
          <br />
          <Button
            variant="contained"
            color="primary"
            disabled={isSubmitting}
            onClick={submitForm}
          >
            Save
          </Button>
        </Form>
      )}
    </Formik>
  );
};
export default CreateInitial;
