import { Card, CardContent, IconButton, Typography } from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { IDroplet } from 'dots-wrapper/dist/modules/droplet';
import React, { useEffect, useMemo, useState } from 'react';
import styled from 'styled-components';

interface ExistingProps {
  droplet: IDroplet;
  destroyExisting: (droplet_id: number) => void;
}

const StyledCardContent = styled(CardContent)`
  display: flex;
  flex-direction: row;
`;
const Flex = styled.div`
  flex: 1;
`;
export default function Existing({ droplet, destroyExisting }: ExistingProps) {
  const startDate = useMemo(() => new Date(droplet.created_at).getTime(), [
    droplet.created_at,
  ]);
  const [now, setNow] = useState(new Date().getTime());
  useEffect(() => {
    const interval = setInterval(() => setNow(new Date().getTime()), 60_000);
    return () => clearInterval(interval);
  }, []);
  const minutesUp = useMemo(() => Math.ceil((now - startDate) / 60_000), [
    now,
    startDate,
  ]);
  return (
    <Card>
      <StyledCardContent>
        <IconButton onClick={() => destroyExisting(droplet.id)} size="small">
          <Delete />
        </IconButton>
        <Flex>
          <Typography variant="h5" noWrap>
            {droplet.name}
          </Typography>
        </Flex>
        <div>
          <Typography
            variant="subtitle1"
            color="textSecondary"
            component="span"
            display="block"
          >{`${Math.floor(minutesUp / 60)}h${Math.round(
            (minutesUp / 60 - Math.floor(minutesUp / 60)) * 60,
          )}m`}</Typography>
          <Typography variant="caption" display="block">
            {`($${Math.round(
              Math.ceil(minutesUp / 60) * droplet.size.price_hourly * 100,
            ) / 100})`}
          </Typography>
        </div>
      </StyledCardContent>
    </Card>
  );
}
