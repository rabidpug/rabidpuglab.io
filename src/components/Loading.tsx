import { CircularProgress, Typography } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';

const Wrap = styled.div`
  position: absolute;
  width: 300px;
  height: 150px;
  top: 50%;
  left: 50%;
  margin-top: -75px;
  margin-left: -150px;
`;
export default function Loading() {
  return (
    <Wrap>
      <CircularProgress />
      <Typography variant="subtitle1" component="h1" align="center">
        Loading...
      </Typography>
    </Wrap>
  );
}
