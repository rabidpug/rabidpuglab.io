import { createApiClient } from 'dots-wrapper';
import {
  ICreateDropletApiRequest,
  IDroplet,
} from 'dots-wrapper/dist/modules/droplet';
import { IImage } from 'dots-wrapper/dist/modules/image';
import { IRegion } from 'dots-wrapper/dist/modules/region';
import { ISize } from 'dots-wrapper/dist/modules/size';
import { ISshKey } from 'dots-wrapper/dist/modules/ssh-key';
import { useEffect, useMemo, useState } from 'react';

import sortByKey from '../utilities/sortByKey';

export interface Options {
  regions: IRegion[];
  sizes: ISize[];
  images: IImage[];
  ssh_keys: ISshKey[];
}
export default function useDO(token?: string) {
  const [existing, setExisting] = useState<IDroplet[]>([]);
  const [options, setOptions] = useState<Options>();
  const [loading, setLoading] = useState(false);
  const api = useMemo(() => {
    if (token) {
      return createApiClient({ token });
    }
  }, [token]);
  useEffect(() => {
    if (api) {
      setLoading(true);
      Promise.all([
        api.droplet.listDroplets({ per_page: 200 }),
        api.region.listRegions({ per_page: 200 }),
        api.size.listSizes({ per_page: 200 }),
        api.image.listImages({ type: 'distribution', per_page: 200 }),
        api.sshKey.listSshKeys({ per_page: 200 }),
      ]).then(
        ([
          {
            data: { droplets },
          },
          {
            data: { regions },
          },
          {
            data: { sizes },
          },
          {
            data: { images },
          },
          {
            data: { ssh_keys },
          },
        ]) => {
          setOptions({
            regions: regions.sort(sortByKey<IRegion>('name')),
            sizes: sizes.sort(sortByKey<ISize>('price_hourly')),
            images: images.sort(sortByKey<IImage>('name')),
            ssh_keys: ssh_keys.sort(sortByKey<ISshKey>('name')),
          });
          setExisting(droplets);
          setLoading(false);
        },
      );
    }
  }, [api]);
  const destroyExisting = (droplet_id: number) => {
    setLoading(true);
    api?.droplet
      .deleteDroplet({ droplet_id })
      .then(() =>
        setExisting(prev => prev.filter(droplet => droplet.id !== droplet_id)),
      )
      .finally(() => setLoading(false));
  };
  const createDroplet = (options: ICreateDropletApiRequest) => {
    setLoading(true);
    api?.droplet
      .createDroplet(options)
      .then(() => api.droplet.listDroplets({ per_page: 200 }))
      .then(({ data: { droplets } }) => {
        setExisting(droplets);
        setLoading(false);
      });
  };
  return { existing, destroyExisting, loading, options, createDroplet };
}
