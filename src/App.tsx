import { Button, Container } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import Create from './components/Create';
import Loading from './components/Loading';
import { apiKey, clientId, discoveryDocs, scope } from './constants';

const StyledContainer = styled(Container)`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  font-size: calc(10px + 2vmin);
  text-align: center;
  background-color: ${({ theme }) => theme.palette.background.default};
`;

function App() {
  const [loading, setLoading] = useState(true);
  const [isSignedIn, setIsSignedIn] = useState(false);
  useEffect(() => {
    gapi.load('client:auth2', () =>
      gapi.client.init({ apiKey, clientId, scope, discoveryDocs }).then(() => {
        setLoading(false);

        gapi.auth2.getAuthInstance().isSignedIn.listen(setIsSignedIn);
        setIsSignedIn(gapi.auth2.getAuthInstance().isSignedIn.get());
      }),
    );
  }, [setLoading]);

  return (
    <StyledContainer maxWidth={false} disableGutters>
      {loading ? (
        <Loading />
      ) : isSignedIn ? (
        <Create />
      ) : (
        <Button
          variant="contained"
          color="primary"
          onClick={() => gapi.auth2.getAuthInstance().signIn()}
        >
          Sign In
        </Button>
      )}
    </StyledContainer>
  );
}

export default App;
