export default function sortByKey<T>(key: keyof T) {
  return function sortByKeySort(x: T, y: T) {
    return x[key] > y[key] ? 1 : x[key] < y[key] ? -1 : 0;
  };
}
